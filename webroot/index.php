
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Site Maintenance</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
  <style>
    html, body { padding: 0; margin: 0; width: 100%; height: 100%; }
    * { box-sizing: border-box; }
    body {
      text-align: center;
      padding: 0;
      font-family: 'Open Sans', sans-serif;
      font-weight: 100;
      font-size: 20px;
      color: #fff;
      display: flex;
      justify-content: center;
      align-items: center;
      background: rgb(2,0,36);
      background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%);
    }
    article { 
      max-width: 90%;
      padding: 20px;
      margin: auto;
    }
    h1 { font-size: 3rem; font-weight: 100; }
    a { color: #fff; font-weight: bold; }
    a:hover { text-decoration: none; }
    svg { width: 75px; margin-top: 1em; }
    ul { list-style: none; padding: 0; }
    ul li { margin-bottom: 10px; }
  </style>
</head>
<body>
  <article>
    <p><img src="https://pub-52838d208b3c484cb5fa90f5691779f4.r2.dev/bbx.jpg" height="200" width="200"></p>
    <h1>Saat ini website dalam pemeliharaan</h1>
    <div>
      <p>Mohon maaf atas ketidaknyamanannya. Kami sedang melakukan pemeliharaan terhadap website ini.</p>
      <p>Kamu juga bisa mengunjungi Media Sosial Kami di:</p>
      <ul>
        <li>Telegram: <a href="https://t.me/hidestresser">https://t.me/hidestresser</a></li>
        <li>Website: <a href="https://brebes-bx.id">https://brebes-bx.id</a></li>
      </ul>
      <p>&copy; 2023<br> Brebes Black Xploit</p>
    </div>
  </article>
</body>
</html>
